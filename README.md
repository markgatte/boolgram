# README #

### What is this repository for? ###

Boolgram is a basic react app to showcase the fundamentals of react.

### How do I get set up? ###

```
npm install
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### How to run tests? ###

```
npm run test
```

Launches the test runner in the interactive watch mode.

### Who do I talk to? ###

https://github.com/mgattello