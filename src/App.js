/* eslint-disable react-hooks/exhaustive-deps */
import './index.css';
import Spinner from './components/Spinner';
import PostList from './components/PostList';
import ProposeUserList from './components/ProposeUserList';
import ProfileList from './components/ProfileList';
import ProfileImage from './components/ProfileImage';

import Navbar from './partials/Navbar';

import { useEffect, useState } from 'react';

// Separation of concerns.
const useInitApp = () => {
  const [profileList, setProfileList] = useState();
  const [postList, setPostList] = useState();

  const options = { crossDomain: true }

  const getProfiles = async () => {
    return await fetch('https://flynn.boolean.careers/exercises/api/boolgram/profiles', options)
      .then(res => res.json())
      .catch(console.log)
  }

  const getFeeds = async () => {
    const res = await fetch('https://flynn.boolean.careers/exercises/api/boolgram/posts', options)
      .then(res => res.json())
      .catch(console.log)
    return res
  }

  useEffect(() => {
    let canceled = false
    getFeeds()
      .then(data => {
        // Avoid racing conditions.
        if (!canceled) {
          setPostList(data)
        }
      })
    getProfiles()
    .then(data => {
        // Avoid racing conditions.
        if (!canceled) {
          setProfileList(data)
        }
      })
    return () => (canceled = true);
  }, [])

  return {
    profileList,
    postList,
    getFeeds,
    getProfiles
  }
}

export const App = () => {
  const {
    profileList,
    postList,
  } = useInitApp();

  return (
    <div className="App">
      <Navbar></Navbar>

      <main className="flex mt-12 gap-14 justify-center">        
        <section className="flex flex-col w-2/4 gap-10">
          <div className="hidden sm:hidden md:hidden xl:flex flex-col p-4 items-center justify-evenly h-40 border rounded-md border-gray-200">
            { profileList ? '' : <Spinner /> }
            <ProfileList profiles={profileList} />
          </div>
          <PostList posts={postList} />
        </section>

        <aside className="flex-col gap-10 hidden sm:hidden md:flex xl:flex">
          <section className="flex gap-10 items-center">
            <ProfileImage custom="w-16 h-16" profilePic="favicon-32x32.png"></ProfileImage>
            <div className="flex flex-col">
              <b>mrj_gatto</b>
              <span className="text-gray-500">Marco Gattello</span>
            </div>
            <div>
              <b className="text-blue-400">Passa a</b>
            </div>
          </section>

          <section className="flex flex-col gap-5 h-40">
            <div className="flex justify-between w-full">
              <span className="text-gray-500">Suggerimenti per te</span>
              <b>Mostra tutti</b>
            </div>
            <ProposeUserList profiles={profileList}/>
          </section>
        </aside>
      </main>
    </div>
  );
}

export default App;
