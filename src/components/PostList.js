import Post from '../partials/Post'

const PostList = ({ posts }) => {
    if (!posts) return <Post />
    return (<section>
        {
            posts.map((post, index) => <Post username={post.profile_fullname} profilePic={post.profile_picture} text={post.post_text} picture={post.post_image} comments={post.comments} likes={post.likes} date={post.date.date} key={'post' + index + post.profile_fullname} />)
        }
    </section>)
}

export default PostList;