const ProfileImage = ({ custom, profilePic }) => {
    if (!profilePic) return <div className={custom + " bg-gray-500 rounded-full"}></div>
    return (<div className={custom + " rounded-full overflow-hidden border-2 border-red-600"}>
        <img className="w-full h-full rounded-full border-2 border-white" src={profilePic} alt='This is a Post pic'></img>
    </div>)
}

export default ProfileImage;