import ProfileImage from './ProfileImage';
import ProfilesPlaceholder from './ProfilePlaceholder';

const ProfileList = ({ profiles }) => {
    if (!profiles) return <ProfilesPlaceholder />
    return (<section className="flex md:gap-6 xl:gap-10">
        {
            profiles.map((profile, index) => {
                if (index >= 6) return ''
                return (<ProfileImage 
                    username={profile.profile_name}
                    profilePic={profile.profile_picture}
                    custom="w-16 h-16"
                    key={index + profile.profile_name}
                ></ProfileImage>)
            })
        }
    </section>)
}

export default ProfileList;