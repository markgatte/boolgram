import ProfileImage from './ProfileImage';

const ProfilesPlaceholder = () => {
    return (<div className="flex md:gap-6 xl:gap-10">
        <ProfileImage custom="w-16 h-16"></ProfileImage>
        <ProfileImage custom="w-16 h-16"></ProfileImage>
        <ProfileImage custom="w-16 h-16"></ProfileImage>
        <ProfileImage custom="w-16 h-16"></ProfileImage>
        <ProfileImage custom="w-16 h-16"></ProfileImage>
    </div>)
}

export default ProfilesPlaceholder;