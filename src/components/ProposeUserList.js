import ProposeUser from '../partials/ProposeUser';

const ProposeUserList = ({ profiles }) => {
    if (!profiles) return ''
    return (<section className="flex flex-col gap-4 justify-between">
        { profiles.map((profile, index) => <ProposeUser username={profile.profile_name} profilePic={profile.profile_picture} key={index + profile} />) }
    </section>)
}

export default ProposeUserList;