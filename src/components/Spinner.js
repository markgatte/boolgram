const Spinner = () => {
    return (<div className="animate-spin relative w-10 h-10 rounded-full bg-gray-200 flex justify-center items-center">
        <div className="w-4 border-t-8 border-r-2 border-b-2 border-l-2 rounded-full border-gray-400 absolute top-0"></div>
        <div className="w-6 h-6 rounded-full bg-white"></div>
    </div>)
}

export default Spinner;

