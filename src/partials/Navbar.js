import React from 'react';
import { HeartIcon } from '@heroicons/react/outline'
import { HomeIcon } from '@heroicons/react/solid'
import ProfileImage from '../components/ProfileImage'

const Navbar = () => {
  return (<nav className="p-3 flex justify-around items-center border border-gray-200">
    <span className="font-grand text-3xl">Boolgram</span>
    <input
      placeholder="Cerca"
      className="appearance-none border border-gray-200 rounded w-60 h-10 py-2 px-3 text-center text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
    />
    <div className="flex gap-4">
      <HeartIcon className="w-7"></HeartIcon>
      <HomeIcon className="w-7"></HomeIcon>
      <ProfileImage custom="w-12 h-12" profilePic="favicon-32x32.png"></ProfileImage>
    </div>
  </nav>)
}

export default Navbar;