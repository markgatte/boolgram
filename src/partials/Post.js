import React from 'react';
import { HeartIcon } from '@heroicons/react/outline'
import { ChatIcon } from '@heroicons/react/outline'
import ProfileImage from '../components/ProfileImage'

const usePost = () => {
  const getDateDifference = (date) => {
    const now = new Date()
    const postDate = new Date(date)
    const rawResult = Math.abs(now - postDate) / (60*60*1000)
    const roundResultToString = Math.round(rawResult).toString()
    const result = `${roundResultToString} Ore fa`
    return result
  }

  return {
    getDateDifference
  }
}

const Post = ({ username, profilePic, text, picture, comments, date, likes }) => {
  // Separation of concerns.
  const {
    getDateDifference
  } = usePost();

  return (<div className="border border-gray-200 rounded-md">
    <div className="flex justify-between items-center p-4">
      <div className="flex gap-5 items-center">
        <ProfileImage profilePic={profilePic} custom="w-12 h-12"></ProfileImage>
        {
          username ?
          <div>{username}</div> :
          <div className="animate-pulse flex flex-col gap-2">
            <div className="h-2 w-28 bg-gray-200"></div>
            <div className="h-2 w-20 bg-gray-200"></div>
          </div>
        }
        
      </div>
      {
        username ?
          <div className="text-2xl self-start">...</div> :
          <div></div>
      }
    </div>
    {
      profilePic ?
        <img className="w-full" src={picture} alt="Post pic" />:
        <div className="w-full h-96 bg-gray-200 animate-pulse"></div>
    }
    { username ? 
      <div className="flex flex-col gap-y-4 p-4">
        <div className="flex gap-5">
          <HeartIcon className="w-8"></HeartIcon>
          <ChatIcon className="w-8"></ChatIcon>
        </div>
        <div className="flex flex-col gap-y-3">
          <div className="flex gap-2">
            {
             !likes ?
              <></> :
              <>
                <ProfileImage 
                  custom="w-6 h-6"
                  profilePic={likes[0].profile_picture}
                ></ProfileImage>
                piace a <b>{likes[0].username}</b> e <b>{likes.length} altri</b>
              </>
            }
          </div>
          <p>
            <b>{username}</b>
            <span className="pl-2">{text}</span>
          </p>
          {
            comments.length <= 1 ?
              <p className="text-gray-500"></p> :
              <p className="text-gray-500">Mostra tutti i {comments.length} commenti</p>
          }
          {
            comments.map((comment, index) => {
              if (index <= 2) {
                return (<p key={new Date().getTime() + index + comment.username}>
                  <b>{comment.username}</b>
                  <span className="pl-2">{comment.text}</span>
                </p>)
              }
              return ''
            })
          }
          <p className="text-gray-500">
            {
              getDateDifference(date)
            }
          </p>
        </div>
      </div> :
      <></>
    }
    <div className="border h-14 border-gray-200 flex items-center justify-between p-4">
      <input className="text-lg appearance-none bordertext-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Aggiungi un commento"/> 
      <b className=" text-blue-400">Pubblica</b>
    </div>
  </div>)
}

export default Post;