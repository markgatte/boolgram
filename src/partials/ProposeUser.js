import ProfileImage from '../components/ProfileImage'

const ProposeUser = ({username, profilePic}) => {
  return (<>
  <section className="flex gap-5 items-center justify-between">
    <div className="flex items-center gap-4">
      <ProfileImage custom="w-12 h-12" profilePic={profilePic}></ProfileImage>
      <b>{username}</b>
    </div>
    <div>
      <b className="text-blue-400">Segui</b>
    </div>
  </section>
</>)
}

export default ProposeUser;